# ICE Portal data collection and things

Continually log data from the ICE portal

## Parts

- `iceportal` is a library crate for the ICE portal's data types.
- `iceportal-server` is a small server that you can use to run a rudimentary simulation of the ICE portal.
- `iceportal-logger` is a tool that writes the information from the ICE portal to a CSV.

## Compiling

To run the tools on your laptop compilation should be straight forward

```console
$ cargo build
```

```console
$ cargo run --bin iceportal-logger
```

And so on.

You can use `cross` to compile the project for a raspberry pi:

```console
$ cross build --target=aarch64-unknown-linux-gnu iceportal-logger
```

Replace the target with an appropriate one if you have a different device.

# To Dos

- [ ] Detect presence of ICE
- [ ] Log data from `https://iceportal.de/api1/rs/tripInfo/trip` 
- [ ] Log to a database.
- [ ] Solve captive portal
