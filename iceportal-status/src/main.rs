// use anyhow::Result;
mod waybar_status;
use chrono::{DateTime, Local};
use clap::Parser;
use iceportal::{error::Result, trip::StopState, Client};
use std::time::{Duration, SystemTime, UNIX_EPOCH, Instant};
use waybar_status::StatusMessage;

// TODO: Handle standing at a platform and not travelling
// TODO: Handle being at a terminus
// TODO: Handle being at a start
// TODO: Add +2 to times
// TODO: check if ETA minutes is correct.

fn format_time_until(real: Option<SystemTime>, schedule: Option<SystemTime>) -> String {
    let now: DateTime<Local> = SystemTime::now().into();
    let time_until = get_time(real, schedule).map(|x| x - now).map(|x| {
        let secs = x.num_seconds();
        let part_min = secs / 60;
        let part_sec = secs.abs() % 60;
        format!("{:02}:{:02}", part_min, part_sec)
    });
    let delay = format_delay(real, schedule).map(|x| x.num_minutes());
    match (time_until, delay) {
        (None, None) => String::new(),
        (None, Some(delay)) => format!("{:+}min", delay),
        (Some(time_until), None) => format!("{}min", time_until),
        (Some(time_until), Some(delay)) => format!("{} ({:+})", time_until, delay),
    }
    // Result is HH:MM+000
}

fn format_stop(stop: &iceportal::trip::Stop) -> String {
    let arrival = format_time_until(stop.timetable.actual_arrival_time.map(|x| UNIX_EPOCH + Duration::from_millis(x)), 
                                    stop.timetable.scheduled_arrival_time.map(|x| UNIX_EPOCH + Duration::from_millis(x)));
    let departure = format_time_until(stop.timetable.actual_departure_time.map(|x| UNIX_EPOCH + Duration::from_millis(x)), 
                                      stop.timetable.scheduled_departure_time.map(|x| UNIX_EPOCH + Duration::from_millis(x)));
    match stop.state() {
        StopState::Past => format!("Arrived at {} {} ago", stop.station.name, arrival),
        StopState::Present => format!("Departing {} in {}", stop.station.name, departure),
        StopState::Future => format!("Arriving at {} in {}", stop.station.name, arrival),
        StopState::Unknown => format!("Next {}", stop.station.name),
    }
}

fn format_combined(status: &iceportal::status::Status, trip: &iceportal::trip::TripInfo) -> String {
    match find_next_stop(&trip) {
        Some(stop) => format!(
            "{}{} to {} | {} | {} km/h",
            trip.trip.train_type,
            trip.trip.vzn,
            trip.trip.stop_info.final_station_name,
            format_stop(&stop),
            status.speed
        ),
        None => format!(
            "{}{} to {} | {} km/h",
            trip.trip.train_type,
            trip.trip.vzn,
            trip.trip.stop_info.final_station_name,
            status.speed
        ),
    }
}

fn format_trip(trip: &iceportal::trip::TripInfo) -> String {
    match find_next_stop(&trip) {
        Some(stop) => format!(
            "{}{} to {} | {}",
            trip.trip.train_type,
            trip.trip.vzn,
            trip.trip.stop_info.final_station_name,
            format_stop(&stop),
        ),
        None => format!(
            "{}{} to {}",
            trip.trip.train_type, trip.trip.vzn, trip.trip.stop_info.final_station_name,
        ),
    }
}

fn format_status(status: &iceportal::status::Status) -> String {
    format!("Tz {} | {} km/h", status.tzn, status.speed)
}

fn find_next_stop(trip: &iceportal::trip::TripInfo) -> Option<&iceportal::trip::Stop> {
    let next_eva = &trip.trip.stop_info.actual_next;
    for stop in &trip.trip.stops {
        if &stop.station.eva_nr == next_eva {
            return Some(stop);
        }
    }
    None
}

fn format_delay(
    real: Option<SystemTime>,
    schedule: Option<SystemTime>,
) -> Option<chrono::Duration> {
    match (real, schedule) {
        (Some(real), Some(schedule)) => {
            let real: DateTime<Local> = real.into();
            let schedule: DateTime<Local> = schedule.into();
            Some(real - schedule)
        }
        _ => None,
    }
}

fn get_time(real: Option<SystemTime>, schedule: Option<SystemTime>) -> Option<DateTime<Local>> {
    match (real, schedule) {
        (Some(time), None) => Some(time),
        (None, None) => None,
        (None, Some(time)) => Some(time),
        (Some(time), Some(_)) => Some(time),
    }
    .map(|x| x.into())
}

fn format_time_pair(real: Option<SystemTime>, schedule: Option<SystemTime>) -> String {
    let time = get_time(real, schedule).map(|time| {
        let local: chrono::DateTime<Local> = time;
        local.format("%H:%M").to_string()
    });
    let delay = format_delay(real, schedule).map(|x| x.num_minutes());
    // Result is HH:MM(+000)
    match (time, delay) {
        (None, None) => String::new(),
        (None, Some(delay)) => format!("     ({:+3})", delay),
        (Some(time), None) => format!("{}     ", time),
        (Some(time), Some(delay)) => format!("{}({:+3})", time, delay),
    }
}

fn get_arrival_time(stop: &iceportal::trip::Stop) -> String {
    format_time_pair(
        stop.timetable
            .actual_arrival_time
            .map(|x| UNIX_EPOCH + Duration::from_millis(x)),
        stop.timetable
            .scheduled_arrival_time
            .map(|x| UNIX_EPOCH + Duration::from_millis(x)),
    )
}

fn get_departure_time(stop: &iceportal::trip::Stop) -> String {
    format_time_pair(
        stop.timetable
            .actual_departure_time
            .map(|x| UNIX_EPOCH + Duration::from_millis(x)),
        stop.timetable
            .scheduled_departure_time
            .map(|x| UNIX_EPOCH + Duration::from_millis(x)),
    )
}

fn format_timetable_entry(stop: &iceportal::trip::Stop) -> String {
    format!(
        "{:<12}{:<12}{}\n",
        get_arrival_time(stop),
        get_departure_time(stop),
        stop.station.name,
    )
}

fn format_timetable(trip: &iceportal::trip::TripInfo) -> String {
    //                          hh:mm:ss hh:mm:ss  blabla
    let mut out = format!("{:<12}{:<12}{}\n", "Arrival", "Departure", "Station");
    for stop in &trip.trip.stops {
        //if stop.info.passed {
        //    continue;
        //}
        out.push_str(&format_timetable_entry(&stop));
    }
    out
}

pub struct State {
    status: Result<iceportal::status::Status>,
    trip: Result<iceportal::trip::TripInfo>,
}

impl State {
    fn get(client: &Client) -> Self {
        let status = client.get_status();
        let trip = client.get_trip();
        State { status, trip }
    }
}

impl StatusMessage for State {
    fn text(&self) -> String {
        match (&self.status, &self.trip) {
            (Ok(status), Ok(trip)) => format_combined(status, trip),
            (Ok(status), Err(_)) => format_status(status),
            (Err(_), Ok(trip)) => format_trip(trip),
            (Err(_), Err(_)) => todo!(),
        }
    }

    fn alt(&self) -> String {
        match &self.status {
            Ok(status) => format_status(status),
            Err(err) => format!("Unable to get status: {}", err),
        }
    }

    fn tooltip(&self) -> String {
        let timetable = match &self.trip {
            Ok(trip) => format_timetable(trip),
            Err(err) => format!("{}", err),
        };
        let status_errors = match &self.status {
            Ok(_) => String::new(),
            Err(err) => format!("{}", err),
        };
        format!("{}\n{}", timetable, status_errors)
    }

    fn percentage(&self) -> Option<f64> {
        Some(0.0)
    }

    fn class(&self) -> Option<String> {
        match (&self.status, &self.trip) {
            (Ok(_), Ok(_)) => None,
            _ => Some(String::from("error")),
        }
    }
}

fn main_loop(client: Client, once: bool) {
    if once {
        println!("{}", State::get(&client).serialize());
    } else {
        let mut state = State::get(&client);
        let mut last_update = Instant::now();
        loop {
            println!("{}", state.serialize());
            std::thread::sleep(Duration::from_secs(1));
            if last_update.elapsed() > Duration::from_secs(10) {
                log::debug!("Getting new status");
                state = State::get(&client);
                last_update = Instant::now();
            }
        }
    }
}

/// Program to log data from the ICE Portal
#[derive(Debug, Parser)]
#[command(author, version, about, long_about)]
struct Args {
    /// ICE Portal host
    #[arg(long, short, default_value = "iceportal.de")]
    address: String,

    /// ICE Portal port
    #[arg(long, short)]
    port: Option<u16>,

    /// Plain HTTP
    #[arg(long, short, default_value_t = false)]
    no_https: bool,

    /// Don't loop
    #[arg(long, short)]
    once: bool,
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let client = Client::new(&args.address, args.port, !args.no_https);
    main_loop(client, args.once);
}
