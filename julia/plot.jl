using Plots
using StatsPlots
using CSV
using Dates
using DataFrames
using DataFramesMeta

push!(ARGS, "2023-12-17_165110_train.csv")

data = CSV.read(ARGS[1], DataFrame)
data[!,"server_time"] = unix2datetime.(data[!,"server_time"] ./ 1000)

data |> names

xlim = (floor(minimum(data[!,"server_time"]), Hour), ceil(maximum(data[!,"server_time"]), Hour))
ticks = (xlim[1]:Hour(1):xlim[2])
ticklabels = Dates.format.(ticks, "HH:MM")
p1 = plot(xlim=xlim, xtick=(ticks, ticklabels), ylabel="Speed (km/h)", xlabel="Time of day", legend=:topleft)
for tzn in unique(data[!,"tzn"])
    @df @subset(data, :tzn .== tzn) plot!(p1, :server_time, :speed, label=tzn)
end
savefig("p1_" * ARGS[1] * ".svg")


p2 = plot(ylabel="", xlabel="", aspect_ratio=1.544)
@df data plot!(p2, :longitude, :latitude, label="", line_z=:speed, color=:thermal, lw=1, msw=0)
@df data scatter!(p2, :longitude, :latitude, label="", marker_z=:speed, color=:thermal, lw=8, msw=0)
p2

savefig("p2_" * ARGS[1] * ".svg")
