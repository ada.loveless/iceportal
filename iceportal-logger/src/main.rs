use chrono::{offset::Utc, DateTime};
use clap::Parser;
use csv;
use iceportal::{
    Client,
    error::{Error, Result},
};
use log;
use std::{
    fs::File,
    thread,
    time::{self, SystemTime, UNIX_EPOCH, Duration},
};

mod status;
mod trip;

struct Logger {
    status_writer: csv::Writer<File>,
    trip_writer: csv::Writer<File>,
    stop_writer: csv::Writer<File>,
    client: Client,
}

fn setup_file(handle: &str) -> Result<csv::Writer<File>> {
    log::info!("Writing in {handle}");
    let file = File::create(handle).map_err(Error::IO)?;
    Ok(csv::Writer::from_writer(file))
}

impl Logger {
    fn new(client: Client, base_path: &str) -> Result<Self> {
        let ts: DateTime<Utc> = time::SystemTime::now().into();
        let ts_str = ts.format("%F_%H-%M-%S");
        let status_file = format!("{}{}{}", base_path, ts_str, "_status.csv");
        let trip_file = format!("{}{}{}", base_path, ts_str, "_trip.csv");
        let stop_file = format!("{}{}{}", base_path, ts_str, "_stops.csv");
        let logger = Logger {
            status_writer: setup_file(&status_file)?,
            trip_writer: setup_file(&trip_file)?,
            stop_writer: setup_file(&stop_file)?,
            client,
        };
        Ok(logger)
    }

    fn write_status(&mut self, status: status::StatusRecord) -> Result<()> {
        let status_record = status::StatusRecord::from(status);
        self.status_writer.serialize(status_record).map_err(|x| Error::Message(format!("Error serialising CSV: {}", x)))?;
        self.status_writer.flush().map_err(Error::IO)?;
        Ok(())
    }

    fn write_trip(&mut self, trip: trip::TripRecord) -> Result<()> {
        let trip_record = trip::TripRecord::from(trip);
        self.trip_writer.serialize(trip_record).map_err(|x| Error::Message(format!("Error serialising CSV: {}", x)))?;
        self.trip_writer.flush().map_err(Error::IO)?;
        Ok(())
    }

    fn write_stops(&mut self, stops: Vec<trip::StopRecord>) -> Result<()> {
        for stop in stops {
            self.stop_writer.serialize(stop).map_err(|x| Error::Message(format!("Error serialising CSV: {}", x)))?;
        }
        self.stop_writer.flush().map_err(Error::IO)?;
        Ok(())
    }

    fn fetch_write_request(&mut self) -> Result<()> {
        let ts = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .map_err(|x| Error::Message(format!("Error related to time: {}", x)))?
            .as_secs();
        let status = self.client.get_status()?;
        let trip = self.client.get_trip()?;
        let mut status_record = status::StatusRecord::from(status);
        let stop_records = trip
            .trip
            .stops
            .clone()
            .unwrap_or(vec![])
            .clone()
            .into_iter()
            .map(|x| {
                let mut record = trip::StopRecord::from(x);
                record.request_ts = Some(ts);
                record
            })
            .collect();
        let mut trip_record = trip::TripRecord::from(trip);
        status_record.request_ts = Some(ts);
        trip_record.request_ts = Some(ts);
        let status_result = self.write_status(status_record);
        let trip_result = self.write_trip(trip_record);
        let stop_result = self.write_stops(stop_records);
        status_result?;
        trip_result?;
        stop_result?;
        Ok(())
    }

    fn request_loop(&mut self, delay_seconds: u64) {
        let mut delay: Duration;
        loop {
            delay = match self.fetch_write_request() {
                Ok(_) => {
                    log::info!("Got data and wrote to file");
                    Duration::from_secs(delay_seconds)
                }
                Err(error) => {
                    log::error!("{error}");
                    match error {
                        Error::Network(_) => Duration::from_secs(5),
                        Error::Parse(_) => Duration::from_secs(60),
                        Error::IO(_) => Duration::from_secs(60),
                        Error::Message(_) => Duration::from_secs(delay_seconds),
                    }
                }
            };
            thread::sleep(delay);
        }
    }
}

/// Program to log data from the ICE Portal
#[derive(Debug, Parser)]
#[command(author, version, about, long_about)]
struct Args {
    /// Output suffix. Will write to stdout if not provided.
    #[arg(long, short, default_value = "train_")]
    base_path: String,

    /// Default delay between requests
    #[arg(long, short, default_value_t = 10)]
    delay: u64,

    /// ICE Portal host
    #[arg(long, short, default_value = "iceportal.de")]
    address: String,

    /// ICE Portal port
    #[arg(long, short)]
    port: Option<u16>,

    /// Plain HTTP
    #[arg(long, short, default_value_t = false)]
    no_https: bool,
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let client = Client::new(&args.address, args.port, !args.no_https);
    let mut logger = match Logger::new(client, &args.base_path) {
        Ok(logger) => logger,
        Err(err) => panic!("Errored creating logger: {err}"),
    };
    logger.request_loop(args.delay);
}
