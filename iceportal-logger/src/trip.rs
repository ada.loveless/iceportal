use iceportal::trip::{Stop, TripInfo};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct TripRecord {
    pub request_ts: Option<u64>,
    pub trip_id: String,
    pub trip_date: Option<String>,
    pub train_type: Option<String>,
    pub vzn: Option<String>,
    pub actual_position: i64,
    pub distance_from_last_stop: i64,
    pub total_distance: i64,
    pub scheduled_next: Option<String>,
    pub actual_next: Option<String>,
    pub actual_last: Option<String>,
    pub actual_last_started: Option<String>,
    pub final_station_name: Option<String>,
    pub final_station_eva_nr: Option<String>,
    // pub stops: Vec<Stop>,
    pub active: Option<bool>,
}

impl From<TripInfo> for TripRecord {
    fn from(other: TripInfo) -> TripRecord {
        TripRecord {
            request_ts: None,
            trip_id: String::from(""),
            trip_date: other.trip.trip_date,
            train_type: other.trip.train_type,
            vzn: other.trip.vzn,
            actual_position: other.trip.actual_position,
            distance_from_last_stop: other.trip.distance_from_last_stop,
            total_distance: other.trip.total_distance,
            scheduled_next: other.trip.stop_info.clone().map(|x| x.scheduled_next),
            actual_next: other.trip.stop_info.clone().map(|x| x.actual_next),
            actual_last: other.trip.stop_info.clone().map(|x| x.actual_last),
            actual_last_started: other.trip.stop_info.clone().map(|x| x.actual_last_started),
            final_station_name: other.trip.stop_info.clone().map(|x| x.final_station_name),
            final_station_eva_nr: other.trip.stop_info.clone().map(|x| x.final_station_eva_nr),
            active: other.active,
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct StopRecord {
    pub request_ts: Option<u64>,
    // struct Station
    pub eva_nr: String,
    pub name: String,
    pub code: Option<String>,
    // struct Geocoordinates
    pub latitude: f64,
    pub longitude: f64,
    // struct Timetable
    pub scheduled_arrival_time: Option<u64>,
    pub actual_arrival_time: Option<u64>,
    pub show_actual_arrival_time: Option<bool>,
    pub arrival_delay: String,
    pub scheduled_departure_time: Option<u64>,
    pub actual_departure_time: Option<u64>,
    pub show_actual_departure_time: Option<bool>,
    pub departure_delay: String,
    // struct Track
    pub track_scheduled: String,
    pub track_actual: String,
    // struct Info
    pub status: i64,
    pub passed: bool,
    pub position_status: Option<String>,
    pub distance: i64,
    pub distance_from_start: i64,
    pub delay_reasons: Option<String>,
}

impl From<Stop> for StopRecord {
    fn from(other: Stop) -> StopRecord {
        let delay_reasons = match other.delay_reasons {
            Some(reasons) => {
                let mut s = String::new();
                for reason in reasons {
                    let reason_str: String = reason.into();
                    s.push_str(&reason_str);
                }
                Some(s)
            }
            None => None,
        };
        StopRecord {
            request_ts: None,
            eva_nr: other.station.eva_nr,
            name: other.station.name,
            code: other.station.code,
            latitude: other.station.geocoordinates.latitude,
            longitude: other.station.geocoordinates.longitude,
            scheduled_arrival_time: other.timetable.scheduled_arrival_time,
            actual_arrival_time: other.timetable.actual_arrival_time,
            show_actual_arrival_time: other.timetable.show_actual_arrival_time,
            arrival_delay: other.timetable.arrival_delay,
            scheduled_departure_time: other.timetable.scheduled_departure_time,
            actual_departure_time: other.timetable.actual_departure_time,
            show_actual_departure_time: other.timetable.show_actual_departure_time,
            departure_delay: other.timetable.departure_delay,
            track_scheduled: other.track.scheduled,
            track_actual: other.track.actual,
            status: other.info.status,
            passed: other.info.passed,
            position_status: other.info.position_status,
            distance: other.info.distance,
            distance_from_start: other.info.distance_from_start,
            delay_reasons,
        }
    }
}
