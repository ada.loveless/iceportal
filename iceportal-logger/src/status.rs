use iceportal::status::Status;
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(from = "Status")]
pub struct StatusRecord {
    pub request_ts: Option<u64>,
    pub connection: bool,
    pub service_level: String,
    pub gps_status: String,
    pub internet: String,
    pub latitude: f64,
    pub longitude: f64,
    pub tile_y: i64,
    pub tile_x: i64,
    pub series: Option<String>,
    pub server_time: u64,
    pub speed: f64,
    pub train_type: String,
    pub tzn: Option<String>,
    pub wagon_class: String,
    pub connectivity_current_state: Option<String>,
    pub connectivity_next_state: Option<String>,
    pub connectivity_remaining_time_seconds: Option<i64>,
    pub bap_installed: bool,
}

impl From<Status> for StatusRecord {
    fn from(other: Status) -> StatusRecord {
        StatusRecord {
            request_ts: None,
            connection: other.connection,
            service_level: other.service_level,
            gps_status: other.gps_status,
            internet: other.internet,
            latitude: other.latitude,
            longitude: other.longitude,
            tile_y: other.tile_y,
            tile_x: other.tile_x,
            series: other.series,
            server_time: other.server_time,
            speed: other.speed,
            train_type: other.train_type,
            tzn: other.tzn,
            wagon_class: other.wagon_class,
            connectivity_current_state: other.connectivity.current_state,
            connectivity_next_state: other.connectivity.next_state,
            connectivity_remaining_time_seconds: other.connectivity.remaining_time_seconds,
            bap_installed: other.bap_installed,
        }
    }
}
