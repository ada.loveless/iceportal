# ICE portal Server

This is a small server that can be used to test various tools for the ICE
portal without being on a train network.

## Usage

```console
$ iceportal-server -h
A small server to test ICE portal applications with.

Usage: iceportal-server [OPTIONS]

Options:
  -a, --address <ADDRESS>  Address to bind to [default: 127.0.0.1]
  -p, --port <PORT>        Port to bind to [default: 3030]
  -s, --status <STATUS>    Status data to load
  -t, --trip <TRIP>        Trip data to load
  -h, --help               Print help
  -V, --version            Print version
```

## Data

The data currently being served is static. You can customise the dataset that
is being served with the `-t` and `-s` flags. Both should point at a `json`
that contains the appropriate data to serve.

