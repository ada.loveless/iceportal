use actix_web::{get, web, App, HttpServer, Responder};
use clap::Parser;
use iceportal::status::{Connectivity, Status};
use iceportal::trip::{
    Connection, Geocoordinates, Info, Station, Stop, StopInfo, Timetable, Track, Trip, TripInfo,
};
use log;
use serde::Deserialize;
use std::fs;

fn default_status_data() -> Status {
    log::info!("Providing default iceportal::status::Status data");
    Status {
        connection: true,
        service_level: String::from("AVAILABLE_SERVICE"),
        gps_status: String::from("VALID"),
        internet: String::from("HIGH"),
        latitude: 47.00,
        longitude: 9.74,
        tile_y: 100,
        tile_x: 10,
        series: String::from("403"),
        server_time: 1702135375830,
        speed: 420.0,
        train_type: String::from("ICE"),
        tzn: String::from("ICE304"),
        wagon_class: String::from("Second"),
        connectivity: Connectivity {
            current_state: Some(String::from("HIGH")),
            next_state: Some(String::from("UNSTABLE")),
            remaining_time_seconds: Some(600),
        },
        bap_installed: true,
    }
}

fn default_trip_data() -> TripInfo {
    log::info!("Providing default iceportal::trip::TripInfo data");
    TripInfo {
        trip: Trip {
            trip_date: String::from("2023-12-24"),
            train_type: String::from("ICE"),
            vzn: String::from("691"),
            actual_position: 272958,
            distance_from_last_stop: 49525,
            total_distance: 806978,
            stop_info: StopInfo {
                scheduled_next: String::from("8000261"),     // Munich
                actual_next: String::from("8000261"),        // Munich
                actual_last: String::from("8000105"),        // Frankfurt (M)
                actual_last_started: String::from("800015"), // Frankfurt (M)
                final_station_name: String::from("München Hbf"),
                final_station_eva_nr: String::from("8000261"), // Munich
            },
            stops: vec![
                Stop {
                    station: Station {
                        eva_nr: String::from("8000105"),
                        name: String::from("Frankfurt(Main)Hbf"),
                        code: None,
                        geocoordinates: Geocoordinates {
                            latitude: 50.107145,
                            longitude: 8.663789,
                        },
                    },
                    timetable: Timetable {
                        scheduled_arrival_time: Some(1702140240000),
                        actual_arrival_time: Some(1702140240000),
                        show_actual_arrival_time: Some(true),
                        arrival_delay: String::from(""),
                        scheduled_departure_time: Some(1702140600000),
                        actual_departure_time: Some(1702140600000),
                        show_actual_departure_time: None,
                        departure_delay: String::from(""),
                    },
                    track: Track {
                        scheduled: String::from("9"),
                        actual: String::from("9"),
                    },
                    info: Info {
                        status: 0,
                        passed: false,
                        position_status: Some(String::from("departed")),
                        distance: 87891,
                        distance_from_start: 446823,
                    },
                    delay_reasons: None,
                },
                Stop {
                    station: Station {
                        eva_nr: String::from("8000261"),
                        name: String::from("München Hbf"),
                        code: None,
                        geocoordinates: Geocoordinates {
                            latitude: 48.140232,
                            longitude: 11.558335,
                        },
                    },
                    timetable: Timetable {
                        scheduled_arrival_time: Some(1702152780000),
                        actual_arrival_time: Some(1702152780000),
                        show_actual_arrival_time: Some(true),
                        arrival_delay: String::from(""),
                        scheduled_departure_time: None,
                        actual_departure_time: None,
                        show_actual_departure_time: None,
                        departure_delay: String::from(""),
                    },
                    track: Track {
                        scheduled: String::from("18"),
                        actual: String::from("18"),
                    },
                    info: Info {
                        status: 0,
                        passed: false,
                        position_status: Some(String::from("future")),
                        distance: 7239,
                        distance_from_start: 806978,
                    },
                    delay_reasons: None,
                },
            ],
        },
        connection: Connection {
            train_type: None,
            vzn: None,
            train_number: None,
            station: None,
            timetable: None,
            track: None,
            info: None,
            stops: None,
            conflict: String::from("NO_CONFLICT"),
        },
        active: None,
    }
}

#[derive(Debug)]
struct State {
    status: Status,
    trip: TripInfo,
}

fn data_from_file<T>(handle: &str) -> T
where
    T: for<'a> Deserialize<'a>,
{
    let content = fs::read_to_string(handle).unwrap();
    let data: T = serde_json::from_str(&content).unwrap();
    log::info!("Loaded {} from {}", std::any::type_name::<T>(), handle);
    data
}

#[derive(Debug, Parser)]
#[command(author, version, about, long_about)]
struct Args {
    /// Address to bind to
    #[arg(short, long, default_value = "127.0.0.1")]
    address: std::net::IpAddr,
    /// Port to bind to
    #[arg(short, long, default_value_t = 3030)]
    port: u16,
    /// Status data to load
    #[arg(short, long)]
    status: Option<String>,
    /// Trip data to load
    #[arg(short, long)]
    trip: Option<String>,
}

#[get("/api1/rs/status")]
async fn api_rs_status(data: web::Data<State>) -> impl Responder {
    web::Json(data.status.clone())
}

#[get("/api1/rs/tripInfo/trip")]
async fn api_rs_tripinfo_trip(data: web::Data<State>) -> impl Responder {
    web::Json(data.trip.clone())
}

#[tokio::main]
async fn main() -> std::io::Result<()>{
    env_logger::init();
    let args = Args::parse();
    log::info!("Listening on port {} at {:?}", args.port, args.address);

    let status_data = match args.status {
        Some(path) => data_from_file(&path),
        None => default_status_data(),
    };
    let trip_data = match args.trip {
        Some(path) => data_from_file(&path),
        None => default_trip_data(),
    };

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(State {
                status: status_data.clone(),
                trip: trip_data.clone(),
            }))
            .service(api_rs_status)
            .service(api_rs_tripinfo_trip)
    })
    .bind((args.address, args.port))?
    .run()
    .await?;

    Ok(())
}
