use iceportal::trip::TripInfo;
use ratatui::{
    layout::{Alignment, Constraint},
    prelude::{symbols, Style},
    style::Stylize,
    text::{Line, Span, Text},
    widgets::{Axis, Block, Cell, Chart, Dataset, GraphType, Paragraph, Row, Table, TableState},
};

use chrono::{DateTime, Local};

use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};

use crate::app::{App, Data};
use crate::cli::Args;

pub struct TimeTable {
    data: Data,
    args: Args,
    state: TableState,
}

impl From<&App> for TimeTable {
    fn from(value: &App) -> Self {
        TimeTable {
            data: value.data.clone(),
            args: value.args.clone(),
            state: TableState::default(),
        }
    }
}

fn format_delay(
    real: Option<SystemTime>,
    schedule: Option<SystemTime>,
) -> Option<chrono::Duration> {
    match (real, schedule) {
        (Some(real), Some(schedule)) => {
            let real: DateTime<Local> = real.into();
            let schedule: DateTime<Local> = schedule.into();
            Some(real - schedule)
        }
        _ => None,
    }
}

fn get_time(real: Option<SystemTime>, schedule: Option<SystemTime>) -> Option<DateTime<Local>> {
    match (real, schedule) {
        (Some(time), None) => Some(time),
        (None, None) => None,
        (None, Some(time)) => Some(time),
        (Some(time), Some(_)) => Some(time),
    }
    .map(|x| x.into())
}

fn format_time_pair(real: Option<SystemTime>, schedule: Option<SystemTime>) -> String {
    let time = get_time(real, schedule).map(|time| {
        let local: chrono::DateTime<Local> = time;
        local.format("%H:%M").to_string()
    });
    let delay = format_delay(real, schedule).map(|x| x.num_minutes());
    // Result is HH:MM(+000)
    match (time, delay) {
        (None, None) => String::new(),
        (None, Some(delay)) => format!("     ({:+3})", delay),
        (Some(time), None) => format!("{}     ", time),
        (Some(time), Some(delay)) => format!("{}({:+3})", time, delay),
    }
}

fn format_stop_name(stop: &iceportal::trip::Stop) -> Cell {
    let style = match stop.info.status {
        1 => Style::new().crossed_out(), // cancelled
        2 => Style::new().green(),       // unplanned
        _ => match stop.state() {
            iceportal::trip::StopState::Past => Style::new().gray(),
            iceportal::trip::StopState::Present => Style::new().bold(),
            iceportal::trip::StopState::Future => Style::new(),
            iceportal::trip::StopState::Unknown => Style::new().red().bold(),
        },
    };
    let text = Text::raw(stop.station.name.clone()).alignment(Alignment::Right);
    Cell::new(text).style(style)
}

fn format_stop_platform(stop: &iceportal::trip::Stop) -> Cell {
    let text = if (stop.track.scheduled == stop.track.actual) {
        Line::from(vec![Span::raw(format!("{}", stop.track.scheduled))])
    } else {
        Line::from(vec![
            Span::raw(format!("{} ", stop.track.actual)).green(),
            Span::raw(format!("{}", stop.track.scheduled)).crossed_out(),
        ])
        //format!("{} {}", stop.track.actual, stop.track.scheduled)
    };
    Cell::new(text)
}

fn format_stop_time(actual: Option<u64>, scheduled: Option<u64>) -> String {
    format_time_pair(
        actual.map(|x| UNIX_EPOCH + Duration::from_millis(x)),
        scheduled.map(|x| UNIX_EPOCH + Duration::from_millis(x)),
    )
}

fn style_stop_time(actual: Option<u64>, scheduled: Option<u64>) -> Style {
    match (actual, scheduled) {
        (None, None) => Style::new(),
        (None, Some(_)) => Style::new(),
        (Some(_), None) => Style::new().bold(),
        (Some(actual), Some(scheduled)) => {
            if actual > scheduled {
                let delay = Duration::from_millis(actual) - Duration::from_millis(scheduled);
                if delay < Duration::from_secs(60) {
                    Style::new().green()
                } else {
                    Style::new().red()
                }
            } else {
                Style::new().green()
            }
        }
    }
}

fn format_stop_arrival(stop: &iceportal::trip::Stop) -> Cell {
    let text = format_stop_time(
        stop.timetable.actual_arrival_time,
        stop.timetable.scheduled_arrival_time,
    );
    let style = if stop.info.status == 1 {
        Style::new().crossed_out()
    } else {
        style_stop_time(
            stop.timetable.actual_arrival_time,
            stop.timetable.scheduled_arrival_time,
        )
    };
    Cell::new(text).style(style)
}

fn format_stop_departure(stop: &iceportal::trip::Stop) -> Cell {
    let text = format_stop_time(
        stop.timetable.actual_departure_time,
        stop.timetable.scheduled_departure_time,
    );
    let style = if stop.info.status == 1 {
        Style::new().crossed_out()
    } else {
        style_stop_time(
            stop.timetable.actual_departure_time,
            stop.timetable.scheduled_departure_time,
        )
    };
    Cell::new(text).style(style)
}

fn rows(timetable: &Option<TripInfo>) -> Vec<Row> {
    let mut rows = vec![];
    if timetable.is_some() {
        let stops = &timetable.as_ref().unwrap().trip.stops;
        for stop in stops {
            log::debug!("Adding stop {}", stop.station.name);
            let row = Row::new(vec![
                format_stop_name(&stop),
                format_stop_platform(&stop),
                format_stop_arrival(&stop),
                format_stop_departure(&stop),
            ]);
            rows.push(row)
        }
    }
    rows
}

impl ratatui::widgets::Widget for TimeTable {
    fn render(self, area: ratatui::prelude::Rect, buf: &mut ratatui::prelude::Buffer)
    where
        Self: Sized,
    {
        let foot = match self.data.last_update {
            Some(instant) => format!("Last update {}s ago", instant.elapsed().as_secs()),
            None => String::from("No data"),
        };
        let max_length_name = &self.data.trip.as_ref().map_or(20, |timetable| {
            let mut length = 20;
            for stop in &timetable.trip.stops {
                if stop.station.name.len() > length {
                    length = stop.station.name.len();
                }
            }
            length
        });
        let table = Table::default()
            .rows(rows(&self.data.trip))
            .widths(&[
                Constraint::Length(*max_length_name as u16),
                Constraint::Length(5),
                Constraint::Length(12),
                Constraint::Length(12),
                Constraint::Fill(1),
            ])
            .header(
                Row::new(vec![
                    Text::raw("Station").alignment(Alignment::Right),
                    Text::raw("Pl."),
                    Text::raw("Arrival"),
                    Text::raw("Departure"),
                ])
                .style(Style::new().bold()),
            )
            .footer(Row::new(vec![foot]))
            .highlight_style(Style::new().reversed())
            .highlight_symbol(">>");
        table.render(area, buf)
    }
}
