mod app;
mod cli;
mod widgets;
mod ui;

use std::{path::Path, fs::{File, OpenOptions}};

use clap::Parser;
use log::LevelFilter;
use simplelog::{WriteLogger, Config};

fn get_log() -> File {
    let path = Path::new("rust.log");
    if path.exists() {
        OpenOptions::new()
            .append(true)
            .open(path)
            .unwrap()
    } else {
        File::create(path)
            .unwrap()
    }
}

fn main() -> anyhow::Result<()> {
    let log = get_log();
    WriteLogger::init(LevelFilter::Trace, Config::default(), log).unwrap();
    let mut term = ui::init()?;
    let mut client = iceportal::Client::new("iceportal.de", None, true);
    let result = app::App::from(cli::Args::parse()).run(&mut term, &mut client);
    ui::restore()?;
    result
}
