use std::time::{Duration, Instant};

use anyhow::Error;
use crossterm::event::{self, Event, KeyCode, KeyEvent, KeyEventKind};
use iceportal::status::Status;
use iceportal::trip::TripInfo;
use iceportal::Client;
use ratatui::widgets::{Paragraph, Widget};
use ratatui::Frame;

type Result<T> = std::result::Result<T, Error>;

use crate::cli::Args;
use crate::ui::Term;
use crate::widgets;

#[derive(PartialEq)]
enum State {
    TimeTable,
    Log,
    Exit,
}

#[derive(Debug, Clone)]
pub struct Data {
    pub status: Option<Status>,
    pub trip: Option<TripInfo>,
    pub last_update: Option<Instant>,
}

pub struct App {
    pub data: Data,
    pub state: State,
    pub args: Args,
}

impl From<Args> for App {
    fn from(value: Args) -> Self {
        App {
            args: value,
            state: State::TimeTable,
            data: Data {
                trip: None,
                status: None,
                last_update: None,
            },
        }
    }
}

impl App {
    pub fn run(&mut self, terminal: &mut Term, client: &mut Client) -> Result<()> {
        self.data.trip = client.get_trip().ok();
        self.data.status = client.get_status().ok();
        self.data.last_update = Some(Instant::now());
        while self.state != State::Exit {
            terminal.draw(|frame| self.render_frame(frame))?;
            self.handle_events(client)?;
            if Duration::from_secs(60) < self.data.last_update.unwrap().elapsed() {
                log::debug!("Updating data");
                self.data.status = client.get_status().ok();
                self.data.last_update = Some(Instant::now());
            }
        }
        Ok(())
    }
    fn render_frame(&self, frame: &mut Frame) {
        frame.render_widget(self, frame.size());
    }
    fn handle_events(&mut self, client: &mut Client) -> Result<()> {
        match event::read()? {
            Event::Key(key_event) if key_event.kind == KeyEventKind::Press => {
                self.handle_key_event(key_event, client)
            }
            _ => {}
        };
        Ok(())
    }
    fn handle_key_event(&mut self, key_event: KeyEvent, client: &mut Client) {
        match key_event.code {
            KeyCode::Char('q') => self.state = State::Exit,
            KeyCode::Char('1') => self.state = State::TimeTable,
            KeyCode::Char('2') => self.state = State::Log,
            KeyCode::Char('r') => self.update(client),
            _ => {}
        }
    }
    fn update(&mut self, client: &mut Client) {
        self.data.status = client.get_status().ok();
        self.data.trip = client.get_trip().ok();
        self.data.last_update = Some(Instant::now());
    }
}

impl Widget for &App {
    fn render(self, area: ratatui::prelude::Rect, buf: &mut ratatui::prelude::Buffer)
    where
        Self: Sized,
    {
        match self.state {
            State::TimeTable => {
                let widget = widgets::TimeTable::from(self);
                widget.render(area, buf)
            }
            State::Log => Paragraph::new("Bla bla bla").render(area, buf),
            State::Exit => Paragraph::new("Exiting").render(area, buf),
        }
    }
}
