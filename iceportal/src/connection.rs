use serde::{Deserialize, Serialize};
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use crate::trip::{Info, Station, Timetable, Track, Stop};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Connection {
    pub train_type: String,
    pub vzn: String,
    pub train_number: String,
    pub station: Station,
    pub timetable: Timetable,
    pub track: Track,
    pub info: Info,
    pub stops: Vec<Stop>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Connections {
    pub connections: Vec<Connection>,
    pub requested_eva_nr: String,
}
