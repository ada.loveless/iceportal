use serde::{Deserialize, Serialize};
use std::time::{UNIX_EPOCH, Duration, SystemTime};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct StopInfo {
    // TODO: Figure out what these fields actually mean.
    pub scheduled_next: String,
    pub actual_next: String,
    pub actual_last: String, // this one...
    pub actual_last_started: String, // ...and this one specifically
    pub final_station_name: String,
    pub final_station_eva_nr: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Geocoordinates {
    pub latitude: f64,
    pub longitude: f64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Station {
    pub eva_nr: String,
    pub name: String,
    // TODO: Hop on a train and figure out what code can be filled with, I only ever saw null.
    pub code: Option<String>,
    pub geocoordinates: Geocoordinates,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Timetable {
    pub scheduled_arrival_time: Option<u64>,
    pub actual_arrival_time: Option<u64>,
    pub show_actual_arrival_time: Option<bool>,
    pub arrival_delay: String,
    pub scheduled_departure_time: Option<u64>,
    pub actual_departure_time: Option<u64>,
    pub show_actual_departure_time: Option<bool>,
    pub departure_delay: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Track {
    pub scheduled: String,
    pub actual: String,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Info {
    pub status: i64,
    pub passed: bool,
    pub position_status: Option<String>,
    pub distance: i64,
    pub distance_from_start: i64,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct DelayReason {
    pub code: String,
    pub text: String,
}

impl Into<String> for DelayReason {
    fn into(self) -> String {
        format!("{}: {};", self.code, self.text)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Stop {
    pub station: Station,
    pub timetable: Timetable,
    pub track: Track,
    pub info: Info,
    pub delay_reasons: Option<Vec<DelayReason>>,
}

pub enum StopState {
    Past,
    Present,
    Future,
    Unknown
}

impl Stop {
    fn best_arrival_time(&self) -> Option<SystemTime> {
        match (self.timetable.actual_arrival_time, self.timetable.scheduled_arrival_time) {
            (Some(time), None) => Some(time),
            (None, None) => None,
            (None, Some(time)) => Some(time),
            (Some(time), Some(_)) => Some(time),
        }.map(|x| UNIX_EPOCH + Duration::from_millis(x))
    }
    fn best_departure_time(&self) -> Option<SystemTime> {
        match (self.timetable.actual_departure_time, self.timetable.scheduled_departure_time) {
            (Some(time), None) => Some(time),
            (None, None) => None,
            (None, Some(time)) => Some(time),
            (Some(time), Some(_)) => Some(time),
        }.map(|x| UNIX_EPOCH + Duration::from_millis(x))
    }
    pub fn state(&self) -> StopState {
        let now = SystemTime::now();
        match (self.best_arrival_time(), self.best_departure_time()) {
            (None, None) => StopState::Unknown,
            (None, Some(time)) => {
                if time > now {
                    StopState::Future
                } else {
                    StopState::Past
                }
            },
            (Some(time), None) => {
                if time > now {
                    StopState::Future
                } else {
                    StopState::Past
                }
            },
            (Some(arrival), Some(departure)) => {
                if arrival > now {
                    StopState::Future
                } else if departure < now {
                    StopState::Past
                } else {
                    StopState::Present
                }
            },
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Trip {
    pub trip_date: Option<String>,
    pub train_type: Option<String>,
    pub vzn: Option<String>, 
    pub actual_position: i64,
    pub distance_from_last_stop: i64,
    pub total_distance: i64,
    pub stop_info: Option<StopInfo>,
    pub stops: Option<Vec<Stop>>,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Connection {
    // TODO: Hop on a train and see what this is.
    pub train_type: Option<String>,
    pub vzn: Option<String>, 
    pub train_number: Option<String>, 
    pub station: Option<Station>, 
    pub timetable: Option<Timetable>, 
    pub track: Option<Track>, 
    pub info: Option<Info>, 
    pub stops: Option<Vec<Stop>>, 
    pub conflict: String, 
}

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct TripInfo {
    pub trip: Trip,
    pub connection: Connection,
    pub active: Option<bool>
}
