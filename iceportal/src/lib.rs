pub mod connection;
pub mod error;
pub mod status;
pub mod trip;

use std::io::Read;

use error::{Error, Result};
use format_serde_error::SerdeError;

fn get(client: &reqwest::blocking::Client, uri: &str) -> Result<reqwest::blocking::Response> {
    log::info!("Getting Status from {}", uri);
    let response = client.get(uri).send().map_err(Error::Network)?;
    Ok(response)
}

#[derive(Debug)]
pub struct Client {
    client: reqwest::blocking::Client,
    pub host: String,
    pub port: Option<u16>,
    pub ssl: bool,
}

impl Client {
    pub fn new(host: &str, port: Option<u16>, ssl: bool) -> Self {
        let client = reqwest::blocking::Client::builder()
            .user_agent("curl/7.87.0") // The iceportal only responds to some user agents apparently. Curl is one of them.
            .build()
            .unwrap();
        Client {
            client,
            host: String::from(host),
            port,
            ssl,
        }
    }

    fn port_str(&self) -> String {
        match self.port {
            Some(value) => format!(":{value}"),
            None => String::new(),
        }
    }

    fn schema(&self) -> &str {
        match self.ssl {
            true => "https",
            false => "http",
        }
    }

    pub fn get_status(&self) -> Result<status::Status> {
        let uri = format!(
            "{}://{}{}/api1/rs/status",
            self.schema(),
            self.host,
            self.port_str()
        );
        let mut response = get(&self.client, &uri)?;
        let mut body = String::new();
        match response.status() {
            reqwest::StatusCode::OK => {
                response.read_to_string(&mut body).map_err(Error::IO)?;
                let data = serde_json::from_str(&body)
                    .map_err(|x| Error::Parse(SerdeError::new(body.to_string(), x)))?;
                Ok(data)
            }
            i => Err(Error::Message(format!(
                "Non-OK status code {:?}:\n {:?}",
                i, body
            ))),
        }
    }

    pub fn get_trip(&self) -> Result<trip::TripInfo> {
        let uri = format!(
            "{}://{}{}/api1/rs/tripInfo/trip",
            self.schema(),
            self.host,
            self.port_str()
        );
        let mut response = get(&self.client, &uri)?;
        let mut body = String::new();
        match response.status() {
            reqwest::StatusCode::OK => {
                response.read_to_string(&mut body).map_err(Error::IO)?;
                let data = serde_json::from_str(&body)
                    .map_err(|x| Error::Parse(SerdeError::new(body.to_string(), x)))?;
                Ok(data)
            }
            i => Err(Error::Message(format!(
                "Non-OK status code {:?}:\n {:?}",
                i, body
            ))),
        }
    }

    pub fn get_connection(&self, station: &str) -> Result<status::Status> {
        let uri = format!(
            "{}://{}{}/api1/rs/tripInfo/connection/{}",
            self.schema(),
            self.host,
            self.port_str(),
            station
        );
        let mut response = get(&self.client, &uri)?;
        let mut body = String::new();
        match response.status() {
            reqwest::StatusCode::OK => {
                response.read_to_string(&mut body).map_err(Error::IO)?;
                let data = serde_json::from_str(&body)
                    .map_err(|x| Error::Parse(SerdeError::new(body.to_string(), x)))?;
                Ok(data)
            }
            i => Err(Error::Message(format!(
                "Non-OK status code {:?}:\n {:?}",
                i, body
            ))),
        }
    }
}
