use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Connectivity {
    pub current_state: Option<String>,
    pub next_state: Option<String>,
    pub remaining_time_seconds: Option<i64>,
}

// TODO: Some of these fields are nullable. Maybe consider turning everything into an Option<T>
#[derive(Debug, Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    pub connection: bool,
    pub service_level: String,
    pub gps_status: String,
    pub internet: String,
    pub latitude: f64,
    pub longitude: f64,
    pub tile_y: i64,
    pub tile_x: i64,
    pub series: Option<String>,
    pub server_time: u64,
    pub speed: f64,
    pub train_type: String,
    pub tzn: Option<String>,
    pub wagon_class: String,
    pub connectivity: Connectivity,
    pub bap_installed: bool,
}


