use std::fmt::Display;
use format_serde_error::SerdeError;

#[derive(Debug)]
pub enum Error {
    Network(reqwest::Error),
    Parse(SerdeError),
    IO(std::io::Error),
    Message(String),
}

pub type Result<T> = std::result::Result<T, Error>;

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::Network(err) => {err.fmt(f)},
            Error::Parse(err) => {err.fmt(f)},
            Error::IO(err) => {err.fmt(f)},
            Error::Message(msg) => write!(f, "{}", msg),
        }
    }
}

